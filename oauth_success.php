<?php

session_start();
include('config.php');
include('autoload.php');
global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, $pm;
$request_token = $_GET['oauth_token'];
$request_token_secret = $_SESSION['oauth_token_secret'];
$verifier = $_GET['oauth_verifier'];
$oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET);
$oauth->setToken($request_token, $request_token_secret);
try {
    $acc_token = $oauth->getAccessToken("https://openapi.etsy.com/v2/oauth/access_token", null, $verifier);
    $access_token = $acc_token['oauth_token'];
    $access_token_secret = $acc_token['oauth_token_secret'];
    $_SESSION['access_token'] = $access_token;
    $_SESSION['access_token_secret'] = $access_token_secret;
} catch (OAuthException $e) {
    error_log($e->getMessage());
    error_log(print_r($oauth->getLastResponse(), true));
    error_log(print_r($oauth->getLastResponseInfo(), true));
    exit;
}
echo "<pre>";
print_r($_SESSION);
echo "<br><a href='index.php'>Return to home</a>"
?>