<?php

session_start();
$access_token = $_SESSION['access_token'];
$access_token_secret = $_SESSION['access_token_secret'];
if (empty($access_token)) {
    header("Location: oauth.php");
    exit;
}
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
include('config.php');
global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET;
require_once "db_new/DatabaseManager.php";
$pm = new DatabaseManager();
$access_token = $_SESSION['access_token'];
$access_token_secret = $_SESSION['access_token_secret'];
echo "<pre>";
//$shippingTemplateId = '59347867575';

$sql = "
SELECT 
products.*, 
item_costs.price_details,
item_measurements.measurement_details, 
item_more_details.details as more_item_details, 
item_specifications.specifications_details, 
item_variation.variation_details, 
seller_profiles.details as seller_profiles 
FROM products
LEFT JOIN item_costs ON products.id = item_costs.item_id
LEFT JOIN item_measurements ON products.id = item_measurements.item_id
LEFT JOIN item_more_details ON products.id = item_more_details.item_id
LEFT JOIN item_specifications ON products.id = item_specifications.item_id
LEFT JOIN item_variation ON products.id = item_variation.item_id
LEFT JOIN seller_profiles ON products.id = seller_profiles.item_id WHERE  products.market_place='etsy' AND products.listing_status='pending' ";
$new_listing_products = $pm->fetchResult($sql);
$pm = new DatabaseManager();
echo "Total items to be list :- " . count($new_listing_products) . "\n";
$total_success_listings = $total_error_listings = 0;
foreach ($new_listing_products as $new_listing_product) {
    $response = createListing($new_listing_product);
    if ($response['status'] = "success") {
        $sql = "UPDATE products SET listing_status='listed', market_place_item_id = '" . $response['listed_item_id'] . "' WHERE id='" . $new_listing_product['id'] . "'";
        $pm->executeQuery($sql);
        if (!empty($new_listing_product['variation_details'])) {
            $variation_add = VariationListingRequest($new_listing_product['variation_details'], $response['listed_item_id']);
            if ($variation_add['status'] == "error") {
                $sql = "INSERT INTO `listing_api_response` (`id`, `product_id`, `response`) VALUES (NULL,'CreateListings', '" . $new_listing_product['id'] . "', '" . addslashes($variation_add['api_response']) . "')";
                $pm->executeQuery($sql);
            }
        }
        $sql = "SELECT image_url FROM item_images WHERE item_id='" . $new_listing_product['id'] . "'";
        $images = $pm->fetchResult($sql);
        if (!empty($images)) {
            $responses = uploadingImages($new_listing_product['id'], $response['listed_item_id'], $images);
            foreach ($responses as $response) {
                if ($response['status'] == "error") {
                    $sql = "INSERT INTO `api_response` (`id`, `call_name`, `item_id`, `response`) VALUES (NULL, 'UploadImages', '" . $new_listing_product['id'] . "', '" . addslashes($response['api_response']) . "')";
                    $pm->executeQuery($sql);
                }
            }
        }
        $total_success_listings++;
    } else {
        $sql = "UPDATE products SET listing_status='error' WHERE id='" . $new_listing_product['id'] . "'";
        $pm->executeQuery($sql);
        $sql = "INSERT INTO `api_response` (`id`, `call_name`, `item_id`, `response`) VALUES (NULL, 'CreateListings', '" . $new_listing_product['id'] . "', '" . addslashes($response['api_response']) . "')";
        $pm->executeQuery($sql);
        $total_error_listings++;
    }
}
$pm->closeConnection();
header("Location: index.php");


function grab_image($url)
{
    $filename = time() . "_" . uniqid() . ".jpg";
    $saveto = dirname(realpath(__FILE__)) . "/temp/";
    $img = file_get_contents($url);
    $http_header = $http_response_header;
    if (!empty($http_header) && $http_header[0] == "HTTP/1.0 200 OK") {
        file_put_contents($saveto . $filename, $img);
        return $filename;
    } else {
        return "";
    }
}

function save_image($img)
{
    $filename = time() . "_" . uniqid() . ".jpg";
    $fullpath = dirname(realpath(__FILE__)) . "/temp/" . $filename;
    $ch = curl_init($img);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    if (file_exists($fullpath)) {
        unlink($fullpath);
    }
    $fp = fopen($fullpath, 'x');
    fwrite($fp, $rawdata);
    fclose($fp);
    $file = dirname(realpath(__FILE__)) . "/temp/" . $filename;
    if (file_exists($file)) {
        return $filename;
    }
    return "";
}

function uploadingImages($product_id, $listing_id, $images)
{
    global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, $access_token, $access_token_secret, $pm;
    $oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET);
    $oauth->enableDebug(false);
    $oauth->setToken($access_token, $access_token_secret);
    $send_images = array();
    $sending_images_name = array();
    foreach ($images as $image) {
        $file_name = save_image($image['image_url']);
        if (!empty($file_name)) {
            if (file_exists($source_file = dirname(realpath(__FILE__)) . "/temp/" . $file_name)) {
                $sending_images_name[] = $file_name;
            }
        }
    }
    $sending_images_name = array_unique($sending_images_name);
    $image_upload_response = array();
    if (!empty($sending_images_name)) {
        $params = array();
        foreach ($sending_images_name as $images) {
            $source_file = dirname(realpath(__FILE__)) . "/temp/" . $images;
            if (file_exists($source_file)) {
                $mimetype = image_type_to_mime_type(exif_imagetype($source_file));
                $params = array(
                    '@image' => '@' . $source_file . ';type=' . $mimetype,
                );
                try {
                    $url = "https://openapi.etsy.com/v2/listings/" . $listing_id . "/images";
                    $oauth->fetch($url, $params, OAUTH_HTTP_METHOD_POST);
                    $json = $oauth->getLastResponse();
                    $image_upload_response[] = array("status" => "success", "api_response" => json_decode($json, true));
                } catch (OAuthException $e) {
                    $image_upload_response[] = array("status" => "error", "api_response" => json_encode($e->getMessage(), true));
                }
            }

        }
        foreach ($sending_images_name as $images) {
            $source_file = dirname(realpath(__FILE__)) . "/temp/" . $images;
            if (file_exists($source_file)) {
                unlink($source_file);
            }
        }

    }
    return $image_upload_response;
}

function createListing($new_listing_product)
{
    global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, $access_token, $access_token_secret, $pm;
    $seller_profiles = json_decode($new_listing_product['seller_profiles']);
    $shipping_profile_id = $seller_profiles->shipping_profile_name;
    $item_costs = json_decode($new_listing_product['price_details']);
    $item_price = $item_costs->offer_price;
    $more_item_details = json_decode($new_listing_product['more_item_details']);
    if (!empty($more_item_details)) {
        $when_made = $more_item_details->when_made;
        $who_made = $more_item_details->who_made;
    }
    $new_listing_product['category'] = 6394;
    $postData = Array(
        'state' => 'draft',
        'title' => $new_listing_product['title'] . " test",
//        'sku' => array($new_listing_product['sku']),
        'description' => $new_listing_product['description'],
        'quantity' => $new_listing_product['quantity'],
        'price' => $item_price,
        'is_supply' => 1,
        'taxonomy_id' => $new_listing_product['category'],
        'when_made' => (!empty($when_made) ? $when_made : ''),
        'who_made' => (!empty($who_made) ? $who_made : ''),
        'shipping_template_id' => (int)$shipping_profile_id,
    );
    try {
        $oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET);
        $oauth->setVersion("1.1");
        $oauth->enableDebug(false);
        $oauth->setToken($access_token, $access_token_secret);
        $data = $oauth->fetch("https://openapi.etsy.com/v2/listings", $postData, OAUTH_HTTP_METHOD_POST);
        $json = $oauth->getLastResponse();
        $response = json_decode($json, true);
        return array("status" => "success", "api_response" => "", "listed_item_id" => $response['results'][0]['listing_id']);
    } catch (OAuthException $e) {
        return array("status" => "error", "api_response" => json_encode($e->getMessage(), true));
    }
}

function VariationListingRequest($variation_details, $listing_id)
{
    global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, $access_token, $access_token_secret;
    $variation_details = json_decode($variation_details);
    if (!empty($variation_details)) {
        $variations_size = array();
        $variations_color = array();
        $variations_array = array();
        foreach ($variation_details->options as $key => $variations) {
            if (trim($variations) == "Size") {
                $variations_array['size'][] = trim($variation_details->values[$key]);
            }
            if (trim($variations) == "Color") {
                $variations_array['color'][] = trim($variation_details->values[$key]);
            }
            if (trim($variations) == "SKU") {
                $variations_array['sku'][] = trim($variation_details->values[$key]);
            }
//            if (trim($variations) == "Image") {
//                $variations_array['image'][] = trim($variation_details->values[$key]);
//            }
            if (trim($variations) == "Quantity") {
                $variations_array['quantity'][] = trim($variation_details->values[$key]);
            }
            if (trim($variations) == "Price") {
                $variations_array['price'][] = trim($variation_details->values[$key]);
            }
        }
        if (!empty($variations_array['sku'])) {
            $i = 0;
            foreach ($variations_array['sku'] as $key => $sku) {
                $offeringarray = array();
                $quantity = trim($variations_array['quantity'][$key]);
                $price = trim($variations_array['price'][$key]);
                $offeringarray['quantity'] = (int)$quantity;
                $offeringarray['price'] = $price;
                $propertvaluearray = array();
                $propertvaluearray['property_id'] = 100;
                $propertvaluearray['property_name'] = 'Size';
                $propertvaluearray['values'] = trim($variations_array['size'][$key]);
                $productarray[$i]['property_values'][0] = $propertvaluearray;
                $productarray[$i]['offerings'][0] = $offeringarray;
                $i++;
            }
            $Variationparams = array(
                'products' => json_encode($productarray),
                'price_on_property' => 100,
                'quantity_on_property' => 100,
                'sku_on_property' => 100
            );
            $url = "https://openapi.etsy.com/v2/listings/$listing_id/inventory";
            try {
                $oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET);
                $oauth->setVersion("1.1");
                $oauth->setToken($access_token, $access_token_secret);
                $data = $oauth->fetch("https://openapi.etsy.com/v2/listings/$listing_id/inventory", $Variationparams, OAUTH_HTTP_METHOD_PUT);
                $data = $oauth->fetch($url);
                $json = $oauth->getLastResponse();
                $response = json_decode($json, true);
                return array("status" => "success", "api_response" => "", "listed_item_id" => $listing_id);
            } catch (OAuthException $e) {
                return array("status" => "error", "api_response" => json_encode($e->getMessage(), true));
            }
        }

    }

}