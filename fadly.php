
<?php

class EtsyFunctions {

    public static function CurlEtsy($method, $url, $param = null, $data = null, $check = null, $listId = null, $listname = null) {
//        if (!$check) {
//            return;
//        }

        $etsyconfig = App\StoreConfig::where('store', '=', 'etsy')->get();
        $etsyconfig = unserialize($etsyconfig[0]['config']);
        $consumer_key = $etsyconfig['conkey'];
        $consumer_secret = $etsyconfig['consecret'];
        $TokenSecret = $etsyconfig['toksecret'];
        $Access_token = $etsyconfig['actoken'];
        $nonce = mt_rand();
        $timestamp = time();
        $params = array(
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => $nonce,
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_timestamp' => $timestamp,
            'oauth_version' => '1.0',
            'oauth_token' => $Access_token,
        );
        if ($method == 'GET') {
            if (isset($param)) {
                $query = http_build_query($param);
                $Geturl = $url . '?' . $query;
                foreach ($param as $key => $value) {
                    $params[$key] = $value;
                }
            } else {
                $Geturl = $url;
            }
        } else if ($method == 'PUT') {
            foreach ($param as $key => $value) {
                $params[$key] = $value;
            }

            $data = http_build_query($param, '', '&');
        }
        ksort($params);
        $sortedParamsByKeyEncodedForm = array();
        foreach ($params as $key => $value) {
            $sortedParamsByKeyEncodedForm[] = rawurlencode($key) . '=' . rawurlencode($value);
        }
        $strParams = implode('&', $sortedParamsByKeyEncodedForm);
        $signatureData = strtoupper($method)
                . '&'
                . rawurlencode($url)
                . '&'
                . rawurlencode($strParams);

        $key = rawurlencode($consumer_secret) . '&' . rawurlencode($TokenSecret);
        $signature = base64_encode(hash_hmac('SHA1', $signatureData, $key, 1));
        $arrHeaders = array(
            'Authorization: OAuth '
            . 'oauth_consumer_key="' . $params['oauth_consumer_key'] . '",'
            . 'oauth_nonce="' . $params['oauth_nonce'] . '",'
            . 'oauth_signature_method="' . $params['oauth_signature_method'] . '",'
            . 'oauth_signature="' . rawurlencode($signature) . '",'
            . 'oauth_timestamp="' . $params['oauth_timestamp'] . '",'
            . 'oauth_version="' . $params['oauth_version'] . '",'
            . 'oauth_token="' . rawurlencode($params['oauth_token']) . '",'
        );
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_HTTPHEADER, $arrHeaders);
        curl_setopt($curl, CURLOPT_CAINFO, public_path() . "/ssl/cacert.pem");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        if ($method !== 'GET') {
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        } else {
            $url = $Geturl;
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        $mxdResponse = curl_exec($curl);
        echo curl_error($curl);
        $array = json_decode($mxdResponse, true);
        if (!isset($array)) {
//            echo '<br><br><br><br><br><br><br><br><br>ETSY ERROR ON :' . $listId . ' IN: ' . $listname . ' : <br>';
//            echo $mxdResponse;
//            die;
            if ($listname == 'ListImage' || $listname == 'CreateListing' || $listname == 'UpdateEtsyListing') {
                return 0;
            }
            echo '<br><br><br><br><br><br><br><br><br>ETSY ERROR ON :' . $listId . ' IN: ' . $listname . ' : <br>';
            print_r($mxdResponse);
            echo '<br><br><br><br><br><br><br><br><br>';
            die;
        } else {
            return $array;
        }
//return $mxdResponse;
//        return $array;
    }

    public static function GetEtsyListings($page = 1) {

        $url = "https://openapi.etsy.com/v2/shops/JahdaJewelers/listings/active"; // change draft to active
        $pagePar = array(
            'page' => $page,
            'limit' => 100,
            'offset' => 0
        );
        $Result = self::CurlEtsy('GET', $url, $pagePar, null, 1, 'nolistid', "GetEtsyListings");
        return $Result;
    }

    public static function DownloadEtsyItems($responseObject, $taskid) {
        app('App\Http\Controllers\SyncController')->CheckStoppedProcess($taskid);
        $data = array();
        foreach ($responseObject['results'] as $listing) {
            $ItemArray = array();
            $ItemArray['task_id'] = $taskid;
            $ItemArray['etsy_id'] = $listing['listing_id'];
            $ItemArray['title'] = html_entity_decode(trim($listing['title']), ENT_QUOTES, 'UTF-8');
            $ItemArray['synced'] = 0;
            $ItemArray['deleted'] = 0;
            array_push($data, $ItemArray);
        }
        \App\EtsyItems::insert($data);
        $next_page = $responseObject['pagination']['next_page'];
        if ($next_page) {
            $nextPageResponse = self::GetEtsyListings($next_page);
            self::DownloadEtsyItems($nextPageResponse, $taskid);
        }
    }

    public static function UpdateEtsyListing($EtsyItemId, $ebayItem) {
        $EbayItem = $ebayItem->toArray();
        $description = $ebayItem['description'];
        $NewDescription = self::GetEtsyDescription($description, $EbayItem);
        $EbayItem = $ebayItem->toArray();
        $postfield = array(
            "description" => $NewDescription,
            "shipping_template_id" => 'Shipping id here'
        );
        if (!$ebayItem['is_variation']) {
            $postfield['price'] = $EbayItem['price'];
            if ($EbayItem['quantity'] !== 1 && $EbayItem['quantity'] < 11) {
                $postfield['quantity'] = 10;
            } else {
                $postfield['quantity'] = $EbayItem['quantity'];
            }
        }
        $url = "https://openapi.etsy.com/v2/listings/$EtsyItemId";
        $array = self::CurlEtsy('PUT', $url, $postfield, null, null, $EtsyItemId, 'UpdateEtsyListing');
        while (!is_array($array)) {
            $array = self::CurlEtsy('PUT', $url, $postfield, null, null, $EtsyItemId, 'UpdateEtsyListing');
        }
        if ($ebayItem['is_variation']) {
            self::AddVariationToItem($EbayItem['item_id'], $EtsyItemId);
        }
    }

    public static function GetEtsyDescription($description, $ebayItem) {
        preg_match_all('#<style>(.*?)</style>#is', $description, $matches, PREG_SET_ORDER);
        foreach ($matches as $match) {
            $description = str_replace($match[0], '', $description);
        }
        $description = str_replace('<br>', "\n", $description);
        $description = str_replace('&nbsp;', ' ', $description);
        $description = str_ireplace("</p>", "\n\n</p>", $description);
        $description = str_ireplace("</span>", "\n\n</span>", $description);
        $description = "Jahda Jewelry Company Phone Support (text message or voice calls): 916-533-6907\n\n" . $description;
        $description = strip_tags($description);
        $description = preg_replace('/^\s+/m', "\n", $description);
        return $description;
    }

    public static function CreateListing($TaskId, $EbayItem) {
        $EbayItem = $EbayItem->toArray();
        $CategoryId1 = $EbayItem['store_category_id1'];
        $CategoryId2 = $EbayItem['store_category_id2'];
        $EtsyCategory = \App\StoreCategoryMap::where('ebay_store_category_id1', '=', $CategoryId1)->where('ebay_store_category_id2', '=', $CategoryId2)->first()->toArray();
        $EtsytaxonomyId = $EtsyCategory['etsy_taxonomy_id'];
        $EtsyShopSectionId = $EtsyCategory['shop_section_id'];
        $description = $EbayItem['description'];
        $NewDescription = self::GetEtsyDescription($description, $EbayItem);
        if ($EbayItem["conditionId"] == 3000) {
            $whenMade = "before_1998";
            $whoMade = "someone_else";
        } else {
            $whoMade = "i_did";
            $whenMade = "made_to_order";
        }
        $Listingparams = array();
        if ($EbayItem['quantity'] !== 1 && $EbayItem['quantity'] < 11) {
            $Listingparams['quantity'] = 10;
        } else {
            $Listingparams['quantity'] = $EbayItem['quantity'];
        }
        $Listingparams['title'] = $EbayItem['title'];
        $Listingparams['price'] = $EbayItem['price'];
        $Listingparams['is_supply'] = 0;
        $Listingparams['when_made'] = $whenMade;
        $Listingparams['description'] = $NewDescription;
        $Listingparams['who_made'] = $whoMade;
        $Listingparams['state'] = 'active'; // active
//        $Listingparams['shipping_template_id'] = "51708719901";//store shipping template
        $Listingparams['taxonomy_id'] = $EtsytaxonomyId;
        $Listingparams['shop_section_id'] = $EtsyShopSectionId; // uncomment this
        $url = "https://openapi.etsy.com/v2/listings";
        $array = self::CurlEtsy('POST', $url, null, $Listingparams, null, 'nolistingid', 'CreateListing');
        while (!is_array($array)) {
            $array = self::CurlEtsy('POST', $url, null, $Listingparams, null, 'nolistingid', 'CreateListing');
        }
        return $array['results'][0]['listing_id'];
    }

    public static function AddVariationToItem($EbayItemId, $EtsyItemId) {
        $item = \ebayFunction::GetEbayItem($EbayItemId);
        $productarray = array();
        $i = 0;
        foreach ($item->Item->Variations->Variation as $variation) {
            $offeringarray = array();
//            $offeringarray['quantity'] = (string)$variation->Quantity;
            $offeringarray['quantity'] = 10;
            $offeringarray['price'] = (string) $variation->StartPrice;
            $propertvaluearray = array();
            $propertvaluearray['property_id'] = 100;
            $propertvaluearray['property_name'] = 'Ring Size';
            $propertvaluearray['values'] = (string) $variation->VariationSpecifics->NameValueList->Value;
            $productarray[$i]['property_values'][0] = $propertvaluearray;
            $productarray[$i]['offerings'][0] = $offeringarray;
            $i++;
        }
        $Variationparams = array(
            'products' => json_encode($productarray),
            'price_on_property' => 100,
//            'quantity_on_property' => 100
        );
        $url = "https://openapi.etsy.com/v2/listings/$EtsyItemId/inventory";
        self::CurlEtsy('PUT', $url, $Variationparams, null, null, $EtsyItemId, 'AddVariationToItem');
    }

    public static function AddImagesToItems($ImageAry, $EtsyItemId) {
        if (!is_dir('tmpimg/')) {
            mkdir('tmpimg/');
        }
        $RPath = 'tmpimg/tmp.jpg';
        $ImageAry = array_slice($ImageAry, 0, 6);
        foreach (array_reverse($ImageAry) as $images) {
            $images = str_replace('$_1.JPG', '$_57.JPG', $images);
            $ImageFile = @file_get_contents($images);
            while ($ImageFile == FALSE) {
                $ImageFile = @file_get_contents($images);
                echo 'errror on ' . $images;
            }
            file_put_contents($RPath, $ImageFile);
            $response = self::ListImage($EtsyItemId, $RPath);
            while (!is_array($response)) {
                $response = self::ListImage($EtsyItemId, $RPath);
            }
            @unlink($RPath);
        }
    }

    private static function ListImage($EtsyItemId, $RPath) {
        $url = "https://openapi.etsy.com/v2/listings/$EtsyItemId/images";
        $imageSize = getImageSize($RPath);
        $imageWidth = $imageSize[0];
        $imageHeight = $imageSize[1];
        if ($imageWidth > 10000 | $imageHeight > 10000) {
            self::resize_image($RPath, 9000);
        }
        $source_file = public_path() . "/$RPath";
        $mimeType = 'image/jpeg';
        $filename = basename($source_file);
        $source_file = new CurlFile($source_file, $mimeType, $filename);
        $data = array("image" => $source_file, "filename" => $filename);
        $response = self::CurlEtsy('POST', $url, null, $data, null, $EtsyItemId, 'ListImage');
        return $response;
    }

    private static function resize_image($file, $w) {
        $imageSize = getImageSize($file);
        $imageWidth = $imageSize[0];
        $imageHeight = $imageSize[1];

        $DESIRED_WIDTH = $w;
        $proportionalHeight = round(($DESIRED_WIDTH * $imageHeight) / $imageWidth);

        $originalImage = imageCreateFromJPEG($file);

        $resizedImage = imageCreateTrueColor($DESIRED_WIDTH, $proportionalHeight);

        imageCopyResampled($resizedImage, $originalImage, 0, 0, 0, 0, $DESIRED_WIDTH + 1, $proportionalHeight + 1, $imageWidth, $imageHeight);
        imageJPEG($resizedImage, $file);

        imageDestroy($originalImage);
        imageDestroy($resizedImage);
    }

    public static function DeleteEtsyImages($EtsyItemId) {
        $url = "https://openapi.etsy.com/v2/listings/$EtsyItemId/images";
        $Result = self::CurlEtsy('GET', $url, null, null, null, $EtsyItemId, 'GetDeleteEtsyImages');
        foreach ($Result['results'] as $etsyImages) {
            $EtsyImageId = $etsyImages['listing_image_id'];
            $DeleteUrl = "https://openapi.etsy.com/v2/listings/$EtsyItemId/images/$EtsyImageId";
            self::CurlEtsy('DELETE', $DeleteUrl, null, null, null, $EtsyItemId, 'DeleteEtsyImages');
        }
    }

}
