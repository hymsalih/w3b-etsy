<?php

session_start();
include('config.php');
include('autoload.php');
global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET;

$oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
$oauth->setToken($_SESSION['access_token'], $_SESSION['access_token_secret']);

$postData = array(
    "title" => "shipping policy one",
    "origin_country_id" => 209,
//    "destination_country_id" => "",
    "primary_cost" => 30.50,
    "secondary_cost" => 15.50,
//    "min_processing_days" => "",
//    "max_processing_days" => ""
);

try {
    $data = $oauth->fetch("https://openapi.etsy.com/v2/shipping/templates", $postData, OAUTH_HTTP_METHOD_POST);
    $json = $oauth->getLastResponse();
    print_r(json_decode($json));
} catch (OAuthException $e) {
    print_r($e->getMessage());
    print_r(print_r($oauth->getLastResponse(), true));
    print_r(print_r($oauth->getLastResponseInfo(), true));
    exit;
}
?>