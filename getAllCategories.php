<?php
/**
 * Created by PhpStorm.
 * User: Salih Mohamed
 * Date: 7/14/2018
 * Time: 2:22 PM
 */

session_start();
include('config.php');
include('autoload.php');
global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET;

$oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, OAUTH_SIG_METHOD_HMACSHA1, OAUTH_AUTH_TYPE_URI);
$oauth->setToken($_SESSION['access_token'], $_SESSION['access_token_secret']);
try {
    $data = $oauth->fetch("https://openapi.etsy.com/v2/taxonomy/categories", null, OAUTH_HTTP_METHOD_GET);
    $json = $oauth->getLastResponse();
    $parent_categories = json_decode($json);
    print_r($parent_categories);
//    foreach ($parent_categories->results as $categories) {
//        print_r($categories);
//        die;
//    }
} catch (OAuthException $e) {
    print_r($e->getMessage());
    print_r(print_r($oauth->getLastResponse(), true));
    print_r(print_r($oauth->getLastResponseInfo(), true));
    exit;
}


