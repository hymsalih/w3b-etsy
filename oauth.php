<?php

session_start();
error_reporting(E_ALL);
include('autoload.php');
include('config.php');
global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET;

$oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET);
$req_token = $oauth->getRequestToken("https://openapi.etsy.com/v2/oauth/request_token?scope=email_r%20listings_w%20listings_r", $AUTH_REDIRECT_URL);
$_SESSION['customer_id'] = 1;
$_SESSION['oauth_token_secret'] = $req_token['oauth_token_secret'];
header('Location:' . $req_token['login_url']);
?>
