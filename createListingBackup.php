<?php

session_start();
$access_token = $_SESSION['access_token'];
$access_token_secret = $_SESSION['access_token_secret'];
if (empty($access_token)) {
    header("Location: oauth.php");
    exit;
}


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


error_reporting(E_ALL);
include('config.php');
global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET;
require_once('db/AppManager.php');
$pm = AppManager::getPM();
$access_token = $_SESSION['access_token'];
$access_token_secret = $_SESSION['access_token_secret'];
echo "<pre>";
//$shippingTemplateId = '59347867575';

$sql = "
SELECT 
products.*, 
item_costs.price_details,
item_measurements.measurement_details, 
item_more_details.details as more_item_details, 
item_specifications.specifications_details, 
item_variation.variation_details, 
seller_profiles.details as seller_profiles 
FROM products
LEFT JOIN item_costs ON products.id = item_costs.item_id
LEFT JOIN item_measurements ON products.id = item_measurements.item_id
LEFT JOIN item_more_details ON products.id = item_more_details.item_id
LEFT JOIN item_specifications ON products.id = item_specifications.item_id
LEFT JOIN item_variation ON products.id = item_variation.item_id
LEFT JOIN seller_profiles ON products.id = seller_profiles.item_id WHERE products.market_place='etsy'";
//LEFT JOIN seller_profiles ON products.id = seller_profiles.item_id WHERE products.listing_status='pending' AND products.market_place='etsy'";
$new_listing_products = $pm->fetchResult($sql);
echo "Total items to be list :- " . count($new_listing_products) . "\n";

$total_success_listings = $total_error_listings = 0;

foreach ($new_listing_products as $new_listing_product) {
//    $response = createListing($new_listing_product);
    $response['status'] = "success";
    $response['listed_item_id'] = 614644596;
    if ($response['status'] = "success") {
        $sql = "UPDATE products SET listing_status='listed', market_place_item_id = '" . $response['listed_item_id'] . "' WHERE id='" . $new_listing_product['id'] . "'";
        $pm->executeQuery($sql);
        if (!empty($new_listing_product['variation_details'])) {
            $variation_update = VariationListingRequest($new_listing_product['variation_details'], $response['listed_item_id']);
        }
        // $total_success_listings++;
    } else {
        $sql = "UPDATE products SET listing_status='error' WHERE id='" . $new_listing_product['id'] . "'";
        $pm->executeQuery($sql);

        //api error loging
        $sql = "INSERT INTO `api_response` (`id`, `call_name`, `item_id`, `response`) VALUES (NULL, 'CreateListings', '" . $new_listing_product['id'] . "', '" . $response['api_response'] . "')";
        $pm->executeQuery($sql);

        $total_error_listings++;
    }
}
echo "\nTotal success listings :- " . $total_success_listings;
echo "\nTotal error listings :- " . $total_error_listings;


function createListing($new_listing_product)
{
    global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, $access_token, $access_token_secret, $pm;
    $seller_profiles = json_decode($new_listing_product['seller_profiles']);
    $shipping_profile_id = $seller_profiles->shipping_profile_name;
    $item_costs = json_decode($new_listing_product['price_details']);
    $item_price = $item_costs->offer_price;
    $more_item_details = json_decode($new_listing_product['more_item_details']);
    if (!empty($more_item_details)) {
        $when_made = $more_item_details->when_made;
        $who_made = $more_item_details->who_made;
    }
    if (!empty($new_listing_product['variation_details'])) {
        $has_variations = 1;
    }
    $postData = Array(
        'state' => 'draft',
        'title' => $new_listing_product['title'] . " test",
        'sku' => array($new_listing_product['sku']),
        'description' => $new_listing_product['description'],
        'quantity' => $new_listing_product['quantity'],
        'price' => $item_price,
        'is_supply' => 1,
        'taxonomy_id' => 6394,
        'when_made' => (!empty($when_made) ? $when_made : ''),
        'who_made' => (!empty($who_made) ? $who_made : ''),
        'shipping_template_id' => (int)$shipping_profile_id,
    );
    try {
        $oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET);
        $oauth->setVersion("1.1");
        $oauth->enableDebug(TRUE);
        $oauth->setToken($access_token, $access_token_secret);
        $data = $oauth->fetch("https://openapi.etsy.com/v2/listings", $postData, OAUTH_HTTP_METHOD_POST);
        $json = $oauth->getLastResponse();
        $response = json_decode($json, true);
        return array("status" => "success", "api_response" => "", "listed_item_id" => $response['results'][0]['listing_id']);
    } catch (OAuthException $e) {
        return array("status" => "error", "api_response" => $e->getMessage());
    }
}

function VariationListingRequest($variation_details, $listing_id)
{
    global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, $access_token, $access_token_secret;
    $variation_details = json_decode($variation_details);
    if (!empty($variation_details)) {
        $variations_size = array();
        $variations_color = array();
        $variations_array = array();
        foreach ($variation_details->options as $key => $variations) {
            if (trim($variations) == "Size") {
                $variations_array['size'][] = trim($variation_details->values[$key]);
            }
            if (trim($variations) == "Color") {
                $variations_array['color'][] = trim($variation_details->values[$key]);
            }
            if (trim($variations) == "SKU") {
                $variations_array['sku'][] = trim($variation_details->values[$key]);
            }
//            if (trim($variations) == "Image") {
//                $variations_array['image'][] = trim($variation_details->values[$key]);
//            }
            if (trim($variations) == "Quantity") {
                $variations_array['quantity'][] = trim($variation_details->values[$key]);
            }
            if (trim($variations) == "Price") {
                $variations_array['price'][] = trim($variation_details->values[$key]);
            }
        }
        if (!empty($variations_array['sku'])) {
            foreach ($variations_array['sku'] as $key => $sku) {
                $propertvaluearray = array();
                $offeringarray = array();
                if (!empty($variations_array['size'][$key])) {
                    $propertvaluearray['property_id'] = 200;
                    $propertvaluearray['property_name'] = 'Size';
                    $propertvaluearray['values'] = trim($variations_array['size'][$key]);
                } else if (!empty($variations_array['color'][$key])) {
                    $propertvaluearray['property_id'] = 100;
                    $propertvaluearray['property_name'] = 'Color';
                    $propertvaluearray['values'] = trim($variations_array['color'][$key]);
                }
                if (!empty($variations_array['price'][$key])) {
                    $offeringarray['price'] = trim($variations_array['price'][$key]);
                }
                if (!empty($variations_array['quantity'][$key])) {
                    $offeringarray['quantity'] = trim($variations_array['quantity'][$key]);
                }
                $productarray['property_values'][] = $propertvaluearray;
                $productarray['offerings'][] = $offeringarray;
                $Variationparams = array(
                    'products' => $productarray,
                    'price_on_property' => 100,
                );
                print_r($Variationparams);
                $url = "https://openapi.etsy.com/v2/listings/$listing_id/inventory";
                try {
                    $oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET);
                    $oauth->setVersion("1.1");
                    $oauth->setToken($access_token, $access_token_secret);
                    $data = $oauth->fetch("https://openapi.etsy.com/v2/listings/614644596/inventory", json_encode($Variationparams), OAUTH_HTTP_METHOD_PUT);
                    $data = $oauth->fetch($url);
                    $json = $oauth->getLastResponse();
                    $response = json_decode($json, true);
                    echo "\n\n\n\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
                    print_r($response);
                    die;
//        return array("status" => "success", "api_response" => "", "listed_item_id" => $response['results'][0]['listing_id']);
                    return array("status" => "success", "api_response" => "", "listed_item_id" => 614644596);
                } catch (OAuthException $e) {
                    print_r($e->getMessage());
                    print_r($oauth->getLastResponse(), true);
                    print_r($oauth->getLastResponseInfo(), true);
                    return array("status" => "error", "api_response" => $e->getMessage());
                }


            }
        }

    }

}


function VariationListingRequestX($postData)
{
    global $OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET, $access_token, $access_token_secret;
    echo $access_token . "\n" . $access_token_secret;
    $productarray = array();
    $i = 0;
    for ($x = 1; $x <= 1; $x++) {
        $offeringarray = array();
        $offeringarray['quantity'] = 10 + $x;
        $offeringarray['price'] = (double)10 * $x;
        $propertvaluearray = array();
        $propertvaluearray['property_id'] = 100;
        $propertvaluearray['property_name'] = 'Ring Size';
        $propertvaluearray['values'] = "M" . $x;
        $productarray[$i]['property_values'][0] = $propertvaluearray;
        $productarray[$i]['offerings'][0] = $offeringarray;
        $i++;
    }
    $Variationparams = array(
        'products' => json_encode($productarray),
        'price_on_property' => 100,
    );


    $url = "https://openapi.etsy.com/v2/listings/614644596/inventory";
    try {
        $oauth = new OAuth($OAUTH_CONSUMER_KEY, $OAUTH_CONSUMER_SECRET);
        $oauth->setVersion("1.1");
        $oauth->enableDebug(TRUE);
        $oauth->setToken($access_token, $access_token_secret);
//        $data = $oauth->fetch("https://openapi.etsy.com/v2/listings/$listing_id/variations", $postData, OAUTH_HTTP_METHOD_PUT);
//        $data = $oauth->fetch("https://openapi.etsy.com/v2/listings/$listing_id/inventory", $postData, OAUTH_HTTP_METHOD_PUT);
//        $data = $oauth->fetch("https://openapi.etsy.com/v2/listings/$listing_id/variations", $postData, OAUTH_HTTP_METHOD_POST);
        $data = $oauth->fetch("https://openapi.etsy.com/v2/listings/614644596/inventory", $Variationparams, OAUTH_HTTP_METHOD_PUT);
        $data = $oauth->fetch($url);
        $json = $oauth->getLastResponse();
        $response = json_decode($json, true);
        echo "\n\n\n\nXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX";
        print_r($response);
//        return array("status" => "success", "api_response" => "", "listed_item_id" => $response['results'][0]['listing_id']);
        return array("status" => "success", "api_response" => "", "listed_item_id" => 614644596);
    } catch (OAuthException $e) {
        print_r($e->getMessage());
        print_r($oauth->getLastResponse(), true);
        print_r($oauth->getLastResponseInfo(), true);
        return array("status" => "error", "api_response" => $e->getMessage());
    }
}